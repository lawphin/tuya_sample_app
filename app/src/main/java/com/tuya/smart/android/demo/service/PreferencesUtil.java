package com.tuya.smart.android.demo.service;

import android.content.Context;

import com.tuya.smart.android.demo.TuyaSmartApp;


public class PreferencesUtil {

    public static int getIntModePrivate(String name, String key, int valueDefault) {
        return TuyaSmartApp.getContext().getSharedPreferences(name, Context.MODE_PRIVATE).getInt(key, valueDefault);
    }

    public static String getStringModePrivate(String name, String key, String valueDefault) {
        return TuyaSmartApp.getContext().getSharedPreferences(name, Context.MODE_PRIVATE).getString(key, valueDefault);
    }

    public static Boolean getBooleanModePrivate(String name, String key, Boolean valueDefault) {
        return TuyaSmartApp.getContext().getSharedPreferences(name, Context.MODE_PRIVATE).getBoolean(key, valueDefault);
    }

    public static void setIntModePrivate(String name, String key, int value) {
        TuyaSmartApp.getContext().getSharedPreferences(name, Context.MODE_PRIVATE)
                .edit()
                .putInt(key, value)
                .apply();
    }

    public static void setStringModePrivate(String name, String key, String value) {
        TuyaSmartApp.getContext().getSharedPreferences(name, Context.MODE_PRIVATE)
                .edit()
                .putString(key, value)
                .apply();
    }

    public static void setBooleanModePrivate(String name, String key, Boolean value) {
        TuyaSmartApp.getContext().getSharedPreferences(name, Context.MODE_PRIVATE)
                .edit()
                .putBoolean(key, value)
                .apply();
    }

//    public static String getHttpAuthorizeHeader() {
//        String token = "Bearer ".concat(AuthorizationCore.getToken());
//        PreferencesUtil.setStringModePrivate("USER", "TOKEN", "token");
//        return token;
//    }
}

package com.tuya.smart.android.demo.service.Remote;

import android.os.Handler;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.tuya.smart.android.demo.RemoteActivity;
import com.tuya.smart.android.demo.service.ApiClient;
import com.tuya.smart.android.demo.service.ApiService;
import com.tuya.smart.android.demo.service.PreferencesCore;

import org.apache.commons.codec.digest.HmacAlgorithms;
import org.apache.commons.codec.digest.HmacUtils;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CatagoryRemote {


    /**
     * result : [{"category_id":"1","category_name":"STB"},{"category_id":"2","category_name":"TV"},{"category_id":"3","category_name":"TV Box"},{"category_id":"4","category_name":"DVD"},{"category_id":"5","category_name":"Air Conditioner"},{"category_id":"6","category_name":"Projector"},{"category_id":"7","category_name":"Audio"},{"category_id":"8","category_name":"Fan"},{"category_id":"9","category_name":"Camera"},{"category_id":"10","category_name":"Light"},{"category_id":"11","category_name":"Air Purifier"},{"category_id":"12","category_name":"Water Heater"}]
     * success : true
     * t : 1602489249750
     */

    private boolean success;
    private long t;
    private List<ResultBean> result;
    /**
     * code : 1004
     * msg : sign invalid
     */

    private int code;
    private String msg;

    public static List<CatagoryRemote> arrayCatagoryRemoteFromData(String str) {

        Type listType = new TypeToken<ArrayList<CatagoryRemote>>() {
        }.getType();

        return new Gson().fromJson(str, listType);
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public long getT() {
        return t;
    }

    public void setT(long t) {
        this.t = t;
    }

    public List<ResultBean> getResult() {
        return result;
    }

    public void setResult(List<ResultBean> result) {
        this.result = result;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public static class ResultBean {
        /**
         * category_id : 1
         * category_name : STB
         */

        private String category_id;
        private String category_name;

        public static List<ResultBean> arrayResultBeanFromData(String str) {

            Type listType = new TypeToken<ArrayList<ResultBean>>() {
            }.getType();

            return new Gson().fromJson(str, listType);
        }

        public String getCategory_id() {
            return category_id;
        }

        public void setCategory_id(String category_id) {
            this.category_id = category_id;
        }

        public String getCategory_name() {
            return category_name;
        }

        public void setCategory_name(String category_name) {
            this.category_name = category_name;
        }
    }

}

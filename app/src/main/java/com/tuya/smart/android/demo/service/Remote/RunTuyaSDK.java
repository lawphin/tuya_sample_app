package com.tuya.smart.android.demo.service.Remote;

import com.tuya.smart.home.sdk.TuyaHomeSdk;
import com.tuya.smart.home.sdk.bean.HomeBean;
import com.tuya.smart.home.sdk.callback.ITuyaGetHomeListCallback;

import java.util.List;

public class RunTuyaSDK{
    Long homeId;

    public void getHomeId(CallBack callback) {
        TuyaHomeSdk.getHomeManagerInstance().queryHomeList(new ITuyaGetHomeListCallback() {
            @Override
            public void onError(String errorCode, String error) {
                System.out.println(error);
            }
            @Override
            public void onSuccess(List<HomeBean> homeBeans) {
                homeId = homeBeans.get(0).getHomeId();
                callback.getHomeId(homeId);
            }
        });
    }
}

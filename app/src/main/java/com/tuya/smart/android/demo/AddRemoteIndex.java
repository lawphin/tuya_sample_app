package com.tuya.smart.android.demo;

public class AddRemoteIndex{
	private Result result;
	private long T;
	private boolean success;

	public void setResult(Result result){
		this.result = result;
	}

	public Result getResult(){
		return result;
	}

	public void setT(long T){
		this.T = T;
	}

	public long getT(){
		return T;
	}

	public void setSuccess(boolean success){
		this.success = success;
	}

	public boolean isSuccess(){
		return success;
	}
}

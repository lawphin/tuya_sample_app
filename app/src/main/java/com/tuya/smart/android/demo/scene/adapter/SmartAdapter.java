package com.tuya.smart.android.demo.scene.adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.kyleduo.switchbutton.SwitchButton;
import com.squareup.picasso.Picasso;
import com.tuya.smart.android.demo.R;
import com.tuya.smart.home.sdk.TuyaHomeSdk;
import com.tuya.smart.home.sdk.bean.scene.SceneBean;
import com.tuya.smart.sdk.api.IResultCallback;

import java.util.ArrayList;
import java.util.List;

import static com.tuya.smart.android.demo.base.presenter.SceneListPresenter.SMART_TYPE_SCENE;


/**
 * create by nielev on 2019-10-28
 */
public class SmartAdapter extends RecyclerView.Adapter<SmartAdapter.SceneViewHolder> {
    private List<SceneBean> datas = new ArrayList<>();
    private int mType;
    private Context mContext;
    private OnExecuteListener mExecuteListener;
    private OnSwitchListener mSwitchListener;

    public SmartAdapter(Context context, int type) {
        mType = type;
        mContext = context;
    }

    public void setData(List<SceneBean> scenes) {

        datas.clear();
        datas.addAll(scenes);
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public SceneViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        SceneViewHolder sceneViewHolder = new SceneViewHolder(View.inflate(mContext, R.layout.layout_scene_item, null));

        return sceneViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull SceneViewHolder viewHolder, int i) {
        final SceneBean sceneBean = datas.get(i);
        Picasso.with(mContext).load(Uri.parse(sceneBean.getBackground())).into(viewHolder.scene_bg);
        viewHolder.tv_title.setText(sceneBean.getName());
        if (mType == SMART_TYPE_SCENE) {
            viewHolder.tv_excute.setVisibility(View.VISIBLE);
            viewHolder.tv_excute.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (null != mExecuteListener) {
                        mExecuteListener.onExecute(sceneBean);
                    }
                }
            });
            viewHolder.swtich.setVisibility(View.GONE);
        } else {
            viewHolder.tv_excute.setVisibility(View.GONE);
            viewHolder.swtich.setVisibility(View.VISIBLE);
            viewHolder.swtich.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (null != mSwitchListener){
                        mSwitchListener.onSwitchAutomation(sceneBean);
                    }
                }
            });
        }

        viewHolder.scene_settng.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialog(sceneBean);
            }
        });

    }

    private void showDialog(SceneBean sceneBean) {
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setTitle(R.string.app_name);
        builder.setMessage("Do you want to delete this scene ?");
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {
                TuyaHomeSdk.newSceneInstance(sceneBean.getId()).deleteScene(new IResultCallback() {
                    @Override
                    public void onSuccess() {
                        Toast.makeText(mContext, "Success", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onError(String errorCode, String errorMessage) {
                        Toast.makeText(mContext, "Failed", Toast.LENGTH_SHORT).show();
                    }
                });
                dialog.dismiss();

            }
        });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }

    public void setOnExecuteListener(OnExecuteListener listener) {
        mExecuteListener = listener;
    }

    public void setOnSwitchListener(OnSwitchListener listener) {
        mSwitchListener = listener;
    }

    public interface OnExecuteListener {
        void onExecute(SceneBean bean);
    }

    public interface OnSwitchListener {
        void onSwitchAutomation(SceneBean bean);
    }

    @Override
    public int getItemCount() {
        return datas.size();
    }

    class SceneViewHolder extends RecyclerView.ViewHolder {
        ImageView scene_bg;
        TextView tv_title;
        TextView tv_excute;
        SwitchButton swtich;
        ImageView scene_settng;

        public SceneViewHolder(@NonNull View itemView) {
            super(itemView);
            scene_bg = itemView.findViewById(R.id.scene_bg);
            tv_title = itemView.findViewById(R.id.tv_title);
            tv_excute = itemView.findViewById(R.id.tv_excute);
            swtich = itemView.findViewById(R.id.swtich);
            scene_settng = itemView.findViewById(R.id.scene_setting);
        }
    }

}

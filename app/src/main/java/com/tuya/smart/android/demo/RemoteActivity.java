package com.tuya.smart.android.demo;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.gson.JsonObject;
import com.tuya.smart.android.demo.service.AccessToken;
import com.tuya.smart.android.demo.service.ApiClient;
import com.tuya.smart.android.demo.service.ApiService;
import com.tuya.smart.android.demo.service.Remote.ApiConstant;
import com.tuya.smart.android.demo.service.Remote.Brand;
import com.tuya.smart.android.demo.service.Remote.CallBack;
import com.tuya.smart.android.demo.service.Remote.CatagoryRemote;
import com.tuya.smart.android.demo.service.Remote.RunTuyaSDK;
import com.tuya.smart.android.user.api.ILoginCallback;
import com.tuya.smart.android.user.bean.User;
import com.tuya.smart.home.sdk.TuyaHomeSdk;
import com.tuya.smart.home.sdk.bean.HomeBean;
import com.tuya.smart.home.sdk.callback.ITuyaGetHomeListCallback;
import com.tuya.smart.home.sdk.callback.ITuyaHomeResultCallback;
import com.tuya.smart.sdk.bean.DeviceBean;

import org.apache.commons.codec.digest.HmacAlgorithms;
import org.apache.commons.codec.digest.HmacUtils;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.http.GET;
import retrofit2.http.Header;

public class RemoteActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {

    private String sign;
    private List<CatagoryRemote.ResultBean> catagoryRemote;
    private Spinner dropdown;
    private Spinner dropdown2;
    private Spinner dropdown3;
    private Spinner dropdown4;
    private Button button;
    private Long homeId;
    private String devId;
    private Integer positionBrand;
    ApiService apiService = ApiClient.getClient().create(ApiService.class);
    public final RunTuyaSDK runTuyaSDK = new RunTuyaSDK();
    JsonObject data = new JsonObject();
    JsonObject dataIn = new JsonObject();
    private EditText nameText;

    private String uid = TuyaHomeSdk.getUserInstance().getUser().getUid();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_remote);

        dropdown = findViewById(R.id.spinner);
        dropdown2 = findViewById(R.id.spinner2);
        dropdown3 = findViewById(R.id.spinner3);
        dropdown4 = findViewById(R.id.spinnerId);
        nameText = findViewById(R.id.editTextTextPersonName);

        button = findViewById(R.id.button);

//        runTuyaSDK.getHomeId(new CallBack() {
//            @Override
//            public void getHomeId(Long data) {
//                data.equals(20);
//            }
//        });

        TuyaHomeSdk.getHomeManagerInstance().queryHomeList(new ITuyaGetHomeListCallback() {
            @Override
            public void onError(String errorCode, String error) {
                // do something
            }

            @Override
            public void onSuccess(List<HomeBean> homeBeans) {
                homeId = homeBeans.get(0).getHomeId();
                getDeviceList(homeId);
            }
        });

        dropdown3.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                dropdown4.setSelection(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        dropdown4.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                dropdown3.setSelection(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                textService();
                Call<AddRemoteIndex> call = apiService.postRemoteIndex(uid, devId, data);
                call.enqueue(new Callback<AddRemoteIndex>() {
                    @Override
                    public void onResponse(Call<AddRemoteIndex> call, Response<AddRemoteIndex> response) {
                        Toast.makeText(getApplicationContext(), "Success", Toast.LENGTH_SHORT).show();
//                        Intent intent = new Intent(getApplicationContext(), RemoteActivity.class);
//                        getApplicationContext().startActivity(intent);
                    }

                    @Override
                    public void onFailure(Call<AddRemoteIndex> call, Throwable t) {
                        Toast.makeText(getApplicationContext(), "Error", Toast.LENGTH_SHORT).show();
                    }
                });

            }
        });

    }

    private void getDeviceList(Long homeId) {
        TuyaHomeSdk.newHomeInstance(homeId).getHomeDetail(new ITuyaHomeResultCallback() {
            @Override
            public void onSuccess(HomeBean homeBean) {
                getRemoteId(homeBean.getDeviceList());
            }

            @Override
            public void onError(String errorCode, String errorMsg) {

            }
        });

    }

    private void getRemoteId(List<DeviceBean> deviceList) {
        for (int i = 0; i < deviceList.size(); i++) {
            String name = deviceList.get(i).getCategoryCode();
            if ("wnykq_1w2b5i17i_1".equals(name)) {
                devId = deviceList.get(i).devId;
                getCatagory(devId);
            } else if ("wf_wnykq".equals(name)) {
                devId = deviceList.get(i).devId;
                getCatagory(devId);
            }
        }
    }

    private void textService() {
        String categoryId = dropdown.getSelectedItem().toString();
        String categoryName = dropdown2.getSelectedItem().toString();
        String brandId = dropdown4.getSelectedItem().toString();
        String brandName = dropdown3.getSelectedItem().toString();
        String name = nameText.getText().toString();

//        JsonObject data = new JsonObject();
//        JsonObject dataIn = new JsonObject();
        dataIn.addProperty("name", name);
        dataIn.addProperty("category_id", categoryId);
        dataIn.addProperty("category_name", categoryName);
        dataIn.addProperty("brand_id", brandId);
        dataIn.addProperty("brand_name", brandName);
        data.add("data", dataIn);
//        data.addProperty("type",1);

        System.out.println(data.toString());
    }

    private void setDropDown(List<CatagoryRemote.ResultBean> categories) {
        List<String> id = new ArrayList<>();
        List<String> name = new ArrayList<>();
        for (int i = 0; i < categories.size(); i++) {
            String cn = categories.get(i).getCategory_name();
            if ("TV".equals(cn) || "Air Conditioner".equals(cn)) {
                name.add(categories.get(i).getCategory_name());
                id.add(categories.get(i).getCategory_id());
            }
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, id);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        dropdown.setAdapter(adapter);

        ArrayAdapter<String> adapter2 = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, name);
        adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        dropdown2.setAdapter(adapter2);

        dropdown.setOnItemSelectedListener(this);
        dropdown2.setOnItemSelectedListener(this);
    }

    public void getCatagory(String devId) {

        Call<CatagoryRemote> call = apiService.getCategories(devId);
        call.enqueue(new Callback<CatagoryRemote>() {
            @Override
            public void onResponse(Call<CatagoryRemote> call, Response<CatagoryRemote> response) {
                Log.d("TAG", "Response = " + response);
                List<CatagoryRemote.ResultBean> categories = response.body().getResult();
                setDropDown(categories);
            }

            @Override
            public void onFailure(Call<CatagoryRemote> call, Throwable t) {
                Log.d("TAG", "Response = " + t.toString());
            }
        });
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        String item = parent.getItemAtPosition(position).toString();
        Toast.makeText(parent.getContext(), "Selected: " + item, Toast.LENGTH_SHORT).show();
        dropdown.setSelection(position);
        dropdown2.setSelection(position);

        if (view == dropdown) {
        }

        String categoryId = dropdown.getSelectedItem().toString();
        getBrand(categoryId);
    }

    private void getBrand(String categoryId) {
        Call<Brand> call = apiService.getBrand(devId, categoryId);
        call.enqueue(new Callback<Brand>() {
            @Override
            public void onResponse(Call<Brand> call, Response<Brand> response) {
                Log.d("TAG", "Response = " + response);
                List<Brand.ResultBean> brand = response.body().getResult();
                setDropDown2(brand);
            }

            @Override
            public void onFailure(Call<Brand> call, Throwable t) {
                Log.d("TAG", "Response = " + t.toString());
            }
        });
    }

    private void setDropDown2(List<Brand.ResultBean> brand) {
        List<String> name = new ArrayList<>();
        List<String> brandId = new ArrayList<>();
        for (int i = 0; i < brand.size(); i++) {
            name.add(brand.get(i).getBrandName());
            brandId.add(brand.get(i).getBrandId());
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, name);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        dropdown3.setAdapter(adapter);

        ArrayAdapter<String> adapter2 = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, brandId);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        dropdown4.setAdapter(adapter2);

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
    }
}
package com.tuya.smart.android.demo.service.Remote;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

public class Brand {


    /**
     * result : [{"brand_id":"592","brand_name":"Huabao"},{"brand_id":"472","brand_name":"Feilu"},{"brand_id":"232","brand_name":"Sanyo"},{"brand_id":"353","brand_name":"Guanyuan"},{"brand_id":"1337","brand_name":"Ricai"},{"brand_id":"354","brand_name":"Booloom"},{"brand_id":"1457","brand_name":"Jinda"},{"brand_id":"2667","brand_name":"Diamond"},{"brand_id":"1577","brand_name":"Shinco"},{"brand_id":"1697","brand_name":"Akira"},{"brand_id":"116","brand_name":"Rasonic"},{"brand_id":"1332","brand_name":"Guoji"},{"brand_id":"117","brand_name":"Chunlan"},{"brand_id":"1452","brand_name":"Yuetu"},{"brand_id":"4962","brand_name":"Apton"},{"brand_id":"1553000000","brand_name":"DOMETIC"},{"brand_id":"11","brand_name":"Korechi"},{"brand_id":"12","brand_name":"Samsung"},{"brand_id":"15","brand_name":"Jensany"},{"brand_id":"17","brand_name":"Changhong"},{"brand_id":"242","brand_name":"Changling"},{"brand_id":"1107","brand_name":"Guqiao"},{"brand_id":"122","brand_name":"Deer"},{"brand_id":"2","brand_name":"Skyworth"},{"brand_id":"1347","brand_name":"Sanzuan"},{"brand_id":"487","brand_name":"Fujitsu"},{"brand_id":"1587","brand_name":"Sunburg"},{"brand_id":"3523","brand_name":"IG"},{"brand_id":"126","brand_name":"General"},{"brand_id":"1102","brand_name":"Guangda"},{"brand_id":"7","brand_name":"Hisense"},{"brand_id":"1342","brand_name":"Rijiang"},{"brand_id":"922","brand_name":"Hyundai"},{"brand_id":"804","brand_name":"TICA"},{"brand_id":"927","brand_name":"SAST"},{"brand_id":"22","brand_name":"Toshiba"},{"brand_id":"27","brand_name":"TCL"},{"brand_id":"4991","brand_name":"Gibson"},{"brand_id":"1242","brand_name":"Keming"},{"brand_id":"2331","brand_name":"Haoer"},{"brand_id":"1362","brand_name":"Shenbao"},{"brand_id":"252","brand_name":"Carrier"},{"brand_id":"1117","brand_name":"Huake"},{"brand_id":"1237","brand_name":"Keer"},{"brand_id":"135","brand_name":"Snowflk"},{"brand_id":"377","brand_name":"Daewoo"},{"brand_id":"1357","brand_name":"S-Ling"},{"brand_id":"1598","brand_name":"MLJD"},{"brand_id":"137","brand_name":"Electrolux"},{"brand_id":"1597","brand_name":"Hualing"},{"brand_id":"138","brand_name":"Yonan"},{"brand_id":"1112","brand_name":"Huagao"},{"brand_id":"4987","brand_name":"Airwell"},{"brand_id":"1912","brand_name":"Premier"},{"brand_id":"4989","brand_name":"Mistral"},{"brand_id":"32","brand_name":"LG"},{"brand_id":"37","brand_name":"Haier"},{"brand_id":"1132","brand_name":"Huifeng"},{"brand_id":"1252","brand_name":"Langge"},{"brand_id":"2462","brand_name":"Fortress"},{"brand_id":"1372","brand_name":"Shuaikang"},{"brand_id":"2217","brand_name":"WUQ"},{"brand_id":"1127","brand_name":"Huanghe"},{"brand_id":"146","brand_name":"Maxe"},{"brand_id":"1247","brand_name":"McQuay"},{"brand_id":"147","brand_name":"Galanz"},{"brand_id":"1367","brand_name":"Shengfeng"},{"brand_id":"148","brand_name":"Rabbit"},{"brand_id":"149","brand_name":"Cheblo"},{"brand_id":"1002","brand_name":"Geer"},{"brand_id":"2333","brand_name":"SmartMi"},{"brand_id":"1122","brand_name":"Huamei"},{"brand_id":"947","brand_name":"Xihu"},{"brand_id":"42","brand_name":"Konka"},{"brand_id":"1142","brand_name":"Jiale"},{"brand_id":"2352","brand_name":"Sears"},{"brand_id":"1262","brand_name":"Meiling"},{"brand_id":"1382","brand_name":"Tianjin"},{"brand_id":"154","brand_name":"Lily"},{"brand_id":"155","brand_name":"Zhongyu"},{"brand_id":"156","brand_name":"Zhuoli"},{"brand_id":"1137","brand_name":"Inyan"},{"brand_id":"158","brand_name":"Sinro"},{"brand_id":"1257","brand_name":"Meidian"},{"brand_id":"159","brand_name":"Sigma"},{"brand_id":"1377","brand_name":"Shining"},{"brand_id":"1012","brand_name":"Corona"},{"brand_id":"52","brand_name":"Philips"},{"brand_id":"57","brand_name":"Sharp"},{"brand_id":"1152","brand_name":"Jiangcheng"},{"brand_id":"160","brand_name":"Xinhuabao"},{"brand_id":"161","brand_name":"Mingxing"},{"brand_id":"1150","brand_name":"BanTian"},{"brand_id":"1392","brand_name":"Panchromatic"},{"brand_id":"163","brand_name":"Boyin"},{"brand_id":"164","brand_name":"Broths"},{"brand_id":"165","brand_name":"Xinle"},{"brand_id":"166","brand_name":"SKG"},{"brand_id":"287","brand_name":"Leader"},{"brand_id":"167","brand_name":"Panda"},{"brand_id":"168","brand_name":"Casarte"},{"brand_id":"169","brand_name":"Risuo"},{"brand_id":"1267","brand_name":"Mingyi"},{"brand_id":"2477","brand_name":"Furi"},{"brand_id":"1387","brand_name":"Tianyuan"},{"brand_id":"1707","brand_name":"Amico"},{"brand_id":"170","brand_name":"Chuangye"},{"brand_id":"171","brand_name":"Xinyingyan"},{"brand_id":"1162","brand_name":"Jinbeijing"},{"brand_id":"1282","brand_name":"Raybo"},{"brand_id":"173","brand_name":"Voton"},{"brand_id":"174","brand_name":"Prince"},{"brand_id":"175","brand_name":"Yudian"},{"brand_id":"176","brand_name":"Taiya"},{"brand_id":"177","brand_name":"Rowa"},{"brand_id":"1157","brand_name":"Justice"},{"brand_id":"1277","brand_name":"Qidi"},{"brand_id":"1397","brand_name":"Tongyi"},{"brand_id":"737","brand_name":"INADA"},{"brand_id":"617","brand_name":"TECO"},{"brand_id":"182","brand_name":"Midea"},{"brand_id":"1172","brand_name":"Jinsong"},{"brand_id":"187","brand_name":"DaiKin"},{"brand_id":"1047","brand_name":"Westinghouse"},{"brand_id":"1167","brand_name":"Jinlu"},{"brand_id":"987","brand_name":"Dongxinbao"},{"brand_id":"747","brand_name":"Dipaer"},{"brand_id":"1000195","brand_name":"DAITSU"},{"brand_id":"748","brand_name":"Jinxing1"},{"brand_id":"1847","brand_name":"Funai"},{"brand_id":"1187","brand_name":"Kangli"},{"brand_id":"192","brand_name":"Aux"},{"brand_id":"2152","brand_name":"Sanken"},{"brand_id":"1182","brand_name":"Jichuan"},{"brand_id":"197","brand_name":"Chigo"},{"brand_id":"992","brand_name":"Dunan"},{"brand_id":"3238","brand_name":"National"},{"brand_id":"632","brand_name":"Goldstar"},{"brand_id":"997","brand_name":"Falanbao"},{"brand_id":"1618","brand_name":"FZM"},{"brand_id":"2948","brand_name":"Onida"},{"brand_id":"97","brand_name":"Gree"},{"brand_id":"1502","brand_name":"Zhongyi"},{"brand_id":"886","brand_name":"Oumeidiqi"},{"brand_id":"647","brand_name":"G.E."},{"brand_id":"1507","brand_name":"Zuodan"},{"brand_id":"1627","brand_name":"Elco"},{"brand_id":"772","brand_name":"Dongao"},{"brand_id":"1512","brand_name":"Changgu"},{"brand_id":"1632","brand_name":"Electra"},{"brand_id":"3139","brand_name":"Sunny"},{"brand_id":"532","brand_name":"Tobo"},{"brand_id":"4907","brand_name":"Fedders"},{"brand_id":"1517","brand_name":"Toyohuifeng"},{"brand_id":"2847","brand_name":"Olympus"},{"brand_id":"1097","brand_name":"Geyang"},{"brand_id":"1092","brand_name":"Dajinxing"},{"brand_id":"782","brand_name":"Donxia"},{"brand_id":"662","brand_name":"NEC"},{"brand_id":"1402","brand_name":"Wanbao"},{"brand_id":"1522","brand_name":"Hicon"},{"brand_id":"2732","brand_name":"Shenhua"},{"brand_id":"667","brand_name":"Aite"},{"brand_id":"547","brand_name":"Playmates"},{"brand_id":"1407","brand_name":"Weiteli"},{"brand_id":"2617","brand_name":"Royalsta"},{"brand_id":"4925","brand_name":"Toyotomi"},{"brand_id":"4926","brand_name":"Banshen"},{"brand_id":"552","brand_name":"Changfei"},{"brand_id":"795","brand_name":"GMCC"},{"brand_id":"1412","brand_name":"Wufeng"},{"brand_id":"1896","brand_name":"Benwin"},{"brand_id":"1532","brand_name":"Shanxing"},{"brand_id":"557","brand_name":"Changfeng"},{"brand_id":"1537","brand_name":"Xinghe"},{"brand_id":"2747","brand_name":"Detail"},{"brand_id":"4264","brand_name":"Century"},{"brand_id":"3050","brand_name":"Sampo"},{"brand_id":"1426","brand_name":"Dizhi"},{"brand_id":"1547","brand_name":"Shuaibao"},{"brand_id":"562","brand_name":"Bole"},{"brand_id":"1667","brand_name":"Weili"},{"brand_id":"322","brand_name":"OLI"},{"brand_id":"2512","brand_name":"Soyea"},{"brand_id":"202","brand_name":"Panasonic"},{"brand_id":"1422","brand_name":"Littleduck"},{"brand_id":"1542","brand_name":"Likaier"},{"brand_id":"2752","brand_name":"MBO"},{"brand_id":"567","brand_name":"Boshigao"},{"brand_id":"1662","brand_name":"GEWEIER"},{"brand_id":"327","brand_name":"Aoke"},{"brand_id":"207","brand_name":"Hitachi"},{"brand_id":"329","brand_name":"ZhuoYue"},{"brand_id":"1427","brand_name":"Xiayang"},{"brand_id":"1682","brand_name":"Trane"},{"brand_id":"330","brand_name":"Richvast"},{"brand_id":"572","brand_name":"BOSCH"},{"brand_id":"1437","brand_name":"Frestec"},{"brand_id":"2405","brand_name":"SKM"},{"brand_id":"2767","brand_name":"Macro"},{"brand_id":"1435","brand_name":"Sunwing"},{"brand_id":"212","brand_name":"Yair"},{"brand_id":"577","brand_name":"Kelon"},{"brand_id":"1432","brand_name":"Serene"},{"brand_id":"1552","brand_name":"Liangyu"},{"brand_id":"2762","brand_name":"Combine"},{"brand_id":"337","brand_name":"Baixue"},{"brand_id":"1672","brand_name":"Aidelong"},{"brand_id":"4944","brand_name":"Pinshang"},{"brand_id":"2782","brand_name":"Mitsubishi Electric"},{"brand_id":"1692","brand_name":"ACL"},{"brand_id":"582","brand_name":"Chuanghua"},{"brand_id":"462","brand_name":"PHILCO"},{"brand_id":"342","brand_name":"JDC"},{"brand_id":"1447","brand_name":"York"},{"brand_id":"2657","brand_name":"Littleswan"},{"brand_id":"2777","brand_name":"Mitsubishi Heavy Industries"},{"brand_id":"102","brand_name":"Aucma"},{"brand_id":"587","brand_name":"Conrowa"},{"brand_id":"346","brand_name":"Misakae"},{"brand_id":"347","brand_name":"Boerka"},{"brand_id":"1442","brand_name":"Yaoma"},{"brand_id":"227","brand_name":"Whirlpool"},{"brand_id":"5008","brand_name":"Ogeneral"}]
     * success : true
     * t : 1602645699191
     */

    @SerializedName("success")
    private boolean success;
    @SerializedName("t")
    private long t;
    @SerializedName("result")
    private List<ResultBean> result;

    public static Brand objectFromData(String str, String key) {

        try {
            JSONObject jsonObject = new JSONObject(str);

            return new Gson().fromJson(jsonObject.getString(str), Brand.class);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return null;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public long getT() {
        return t;
    }

    public void setT(long t) {
        this.t = t;
    }

    public List<ResultBean> getResult() {
        return result;
    }

    public void setResult(List<ResultBean> result) {
        this.result = result;
    }

    public static class ResultBean {
        /**
         * brand_id : 592
         * brand_name : Huabao
         */

        @SerializedName("brand_id")
        private String brandId;
        @SerializedName("brand_name")
        private String brandName;

        public static ResultBean objectFromData(String str, String key) {

            try {
                JSONObject jsonObject = new JSONObject(str);

                return new Gson().fromJson(jsonObject.getString(str), ResultBean.class);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }

        public String getBrandId() {
            return brandId;
        }

        public void setBrandId(String brandId) {
            this.brandId = brandId;
        }

        public String getBrandName() {
            return brandName;
        }

        public void setBrandName(String brandName) {
            this.brandName = brandName;
        }
    }
}

package com.tuya.smart.android.demo.service;

import android.app.IntentService;
import android.content.Intent;
import android.content.Context;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.tuya.smart.android.demo.AddRemoteIndex;
import com.tuya.smart.android.demo.service.Remote.ApiConstant;
import com.tuya.smart.android.demo.service.Remote.Brand;
import com.tuya.smart.android.demo.service.Remote.CatagoryRemote;

import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ApiService {

//    @GET("/v1.0/token?grant_type=1")
//    Call<AccessToken> accessToken(
//            @Header("client_id") String clientid,
//            @Header("sign") String sign,
//            @Header("t") String timeStamp,
//            @Header("sign_method") String signMethod
//    );

    @GET("/api/v1/devices/infrareds/{infrared-id}/categories")
    Call<CatagoryRemote> getCategories(
            @Path("infrared-id") String infraredId
    );

    @GET("/api/v1/devices/infrareds/{infrared-id}/categories/{category-id}/brands")
    Call<Brand> getBrand(
            @Path("infrared-id") String infraredId,
            @Path("category-id") String categoryId
    );


    @POST("/api/v1/devices/{uid}/infrareds/{infraredId}/remote-index")
    Call<AddRemoteIndex> postRemoteIndex(
            @Path("uid")String uid,
            @Path("infraredId") String infraredId,
            @Body JsonObject body
    );


}

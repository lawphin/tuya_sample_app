package com.tuya.smart.android.demo;

public class PreferencesCore {
    final public static String currentShipmentCode = "currentShipmentCode";
    final public static String showButtonInDestionation = "showButtonInDestionation";
    final public static String currentDestinationCode = "currentDestinationCode";
    final public static String shipmentButton = "shipmentButton";
    final public static String attachCode = "attachCode";

    public static String getCountryCode() {
        return PreferencesUtil.getStringModePrivate("SHIPMENT", currentShipmentCode, "");
    }

    public static void setCountryCode(String value) {
        PreferencesUtil.setStringModePrivate("SHIPMENT", currentDestinationCode, value);
    }

    public static String getUserName() {
        return PreferencesUtil.getStringModePrivate("SHIPMENT", currentShipmentCode, "");
    }

    public static void setUserName(String value) {
        PreferencesUtil.setStringModePrivate("SHIPMENT", currentDestinationCode, value);
    }

    public static String getPassword() {
        return PreferencesUtil.getStringModePrivate("SHIPMENT", currentShipmentCode, "");
    }

    public static void setPassword(String value) {
        PreferencesUtil.setStringModePrivate("SHIPMENT", currentDestinationCode, value);
    }

}

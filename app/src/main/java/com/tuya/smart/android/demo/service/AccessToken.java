package com.tuya.smart.android.demo.service;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.tuya.smart.android.demo.service.Remote.CatagoryRemote;

import org.apache.commons.codec.digest.HmacAlgorithms;
import org.apache.commons.codec.digest.HmacUtils;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AccessToken {

    /**
     * result : {"access_token":"9494399bee2466ab6c869cbc67194783","expire_time":7200,"refresh_token":"e514a6076ef62df1dfcde8af3d7bc7e4","uid":"bay15991139204291uAi"}
     * success : true
     * t : 1602472118023
     */

    private ResultBean result;
    private boolean success;
    private long t;
    /**
     * code : 1004
     * msg : sign invalid
     */

    private int code;
    private String msg;

    public static List<AccessToken> arrayAccessTokenFromData(String str) {

        Type listType = new TypeToken<ArrayList<AccessToken>>() {
        }.getType();

        return new Gson().fromJson(str, listType);
    }

    public ResultBean getResult() {
        return result;
    }

    public void setResult(ResultBean result) {
        this.result = result;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public long getT() {
        return t;
    }

    public void setT(long t) {
        this.t = t;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public static class ResultBean {
        /**
         * access_token : 9494399bee2466ab6c869cbc67194783
         * expire_time : 7200
         * refresh_token : e514a6076ef62df1dfcde8af3d7bc7e4
         * uid : bay15991139204291uAi
         */

        private String access_token;
        private int expire_time;
        private String refresh_token;
        private String uid;

        public static List<ResultBean> arrayResultBeanFromData(String str) {

            Type listType = new TypeToken<ArrayList<ResultBean>>() {
            }.getType();

            return new Gson().fromJson(str, listType);
        }

        public String getAccess_token() {
            return access_token;
        }

        public void setAccess_token(String access_token) {
            this.access_token = access_token;
        }

        public int getExpire_time() {
            return expire_time;
        }

        public void setExpire_time(int expire_time) {
            this.expire_time = expire_time;
        }

        public String getRefresh_token() {
            return refresh_token;
        }

        public void setRefresh_token(String refresh_token) {
            this.refresh_token = refresh_token;
        }

        public String getUid() {
            return uid;
        }

        public void setUid(String uid) {
            this.uid = uid;
        }
    }
}
